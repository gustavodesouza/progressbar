package com.example.progressbars;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.example.progressbars.databinding.ActivityMain2Binding;
import com.example.progressbars.databinding.ActivityMainBinding;

public class MainActivity2 extends AppCompatActivity implements View.OnClickListener {

    private ActivityMain2Binding mVwBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVwBinding = ActivityMain2Binding.inflate(getLayoutInflater());
        setContentView(mVwBinding.getRoot());

        mVwBinding.button2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        super.onBackPressed();

    }
}