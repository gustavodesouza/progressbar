package com.example.progressbars;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.core.view.ViewCompat;

public class AceProgressBar {

    private int MAX_VALUE_PERCENTAGE    = 100;
    private int PADDING_VALUE_FILL      = 4;
    private int TEXT_LABEL_SIZE         = 20;
    private int FILL_COLOR_BACKGROUND   = R.color.colorPrimary;

    private Context mContext;
    private TextView mTextViewLabelIn, mTextViewLabelOut;
    private View mViewResizeFill, mViewGuideFillSize;
    private int         mSizeFillWidth = 0;

    public AceProgressBar(Context context) {
        mContext = context;
    }

    public void createView(ConstraintLayout constraintLayout, View viewBorder) {

        int sizeViewFill = parsesizeInDP(PADDING_VALUE_FILL);
        ConstraintLayout.LayoutParams paramsViewFill = new ConstraintLayout.LayoutParams(0, 0);
        paramsViewFill.setMargins(sizeViewFill, sizeViewFill, sizeViewFill, sizeViewFill);

        mViewResizeFill = new View(mContext);
        mViewResizeFill.setId(View.generateViewId());
        mViewResizeFill.setLayoutParams(paramsViewFill);
        mViewResizeFill.setBackgroundResource(FILL_COLOR_BACKGROUND);

        ConstraintLayout.LayoutParams paramsFillSize = new ConstraintLayout.LayoutParams(0, 1);
        paramsFillSize.setMargins(parsesizeInDP(PADDING_VALUE_FILL), 0, parsesizeInDP(PADDING_VALUE_FILL),0);

        mViewGuideFillSize = new View(mContext);
        mViewGuideFillSize.setId(View.generateViewId());
        mViewGuideFillSize.setLayoutParams(paramsFillSize);
        mViewGuideFillSize.setBackgroundColor(Color.TRANSPARENT);

        mTextViewLabelIn = new TextView(mContext);
        mTextViewLabelIn.setId(View.generateViewId());
        ViewCompat.setElevation(mTextViewLabelIn, 4F);
        mTextViewLabelIn.setTextColor(Color.WHITE);
        mTextViewLabelIn.setTypeface(mTextViewLabelIn.getTypeface(), Typeface.BOLD);
        mTextViewLabelIn.setVisibility(View.GONE);
        mTextViewLabelIn.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_LABEL_SIZE);

        mTextViewLabelOut = new TextView(mContext);
        mTextViewLabelOut.setId(View.generateViewId());
        ViewCompat.setElevation(mTextViewLabelOut, 4F);
        mTextViewLabelOut.setPadding(12, 0, 0, 0);
        mTextViewLabelOut.setTypeface(mTextViewLabelOut.getTypeface(), Typeface.BOLD);
        mTextViewLabelOut.setTextSize(TypedValue.COMPLEX_UNIT_SP, TEXT_LABEL_SIZE);

        ConstraintSet constraintSet = new ConstraintSet();

        constraintLayout.addView(mViewResizeFill);
        constraintLayout.addView(mViewGuideFillSize);
        constraintLayout.addView(mTextViewLabelIn);
        constraintLayout.addView(mTextViewLabelOut);

        constraintSet.clone(constraintLayout);

        constraintSet.connect(mViewResizeFill.getId(),      ConstraintSet.BOTTOM,   viewBorder.getId(),     ConstraintSet.BOTTOM);
        constraintSet.connect(mViewResizeFill.getId(),      ConstraintSet.START,    viewBorder.getId(),     ConstraintSet.START);
        constraintSet.connect(mViewResizeFill.getId(),      ConstraintSet.TOP,      viewBorder.getId(),     ConstraintSet.TOP);

        constraintSet.connect(mViewGuideFillSize.getId(),   ConstraintSet.BOTTOM,   viewBorder.getId(),     ConstraintSet.BOTTOM);
        constraintSet.connect(mViewGuideFillSize.getId(),   ConstraintSet.END,      viewBorder.getId(),     ConstraintSet.END);
        constraintSet.connect(mViewGuideFillSize.getId(),   ConstraintSet.START,    viewBorder.getId(),     ConstraintSet.START);
        constraintSet.connect(mViewGuideFillSize.getId(),   ConstraintSet.TOP,      viewBorder.getId(),     ConstraintSet.TOP);

        constraintSet.connect(mTextViewLabelIn.getId(),     ConstraintSet.BOTTOM,   mViewResizeFill.getId(), ConstraintSet.BOTTOM);
        constraintSet.connect(mTextViewLabelIn.getId(),     ConstraintSet.END,      mViewResizeFill.getId(), ConstraintSet.END);
        constraintSet.connect(mTextViewLabelIn.getId(),     ConstraintSet.START,    mViewResizeFill.getId(), ConstraintSet.START);
        constraintSet.connect(mTextViewLabelIn.getId(),     ConstraintSet.TOP,      mViewResizeFill.getId(), ConstraintSet.TOP);

        constraintSet.connect(mTextViewLabelOut.getId(),    ConstraintSet.BOTTOM,   mViewResizeFill.getId(), ConstraintSet.BOTTOM);
        constraintSet.connect(mTextViewLabelOut.getId(),    ConstraintSet.START,    mViewResizeFill.getId(), ConstraintSet.END);
        constraintSet.connect(mTextViewLabelOut.getId(),    ConstraintSet.TOP,      mViewResizeFill.getId(), ConstraintSet.TOP);

        constraintSet.applyTo(constraintLayout);

        getParansOnEndCreate(viewBorder, false);
        getParansOnEndCreate(mViewGuideFillSize, true);
    }

    public void updateUI(int percentageValue){
        if(percentageValue >= MAX_VALUE_PERCENTAGE){
            mViewResizeFill.getLayoutParams().width = mSizeFillWidth;
            percentageValue = MAX_VALUE_PERCENTAGE;
        }else if (mViewResizeFill.getWidth() <= mSizeFillWidth) {
            mViewResizeFill.getLayoutParams().width = calculatePercentageFill(percentageValue, mSizeFillWidth);
        }
        mViewResizeFill.requestLayout();

        getParansOnEndCreate(mViewGuideFillSize, true);
        setValueLabelText(percentageValue);
    }

    public void setPaddingValueFill(int value) {
        PADDING_VALUE_FILL = value;
    }

    public void setTextLabelSize(int value) {
        TEXT_LABEL_SIZE = value;
    }

    public void setFillColorBackground(@DrawableRes int color) {
        FILL_COLOR_BACKGROUND = color;
    }

    private int parsesizeInDP(int value){
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, value, mContext.getResources().getDisplayMetrics());
    }

    private int calculatePercentageFill(int value, int sizeFillWidth) {
        mSizeFillWidth = sizeFillWidth;
        return (sizeFillWidth * value)/MAX_VALUE_PERCENTAGE;
    }

    private void getParansOnEndCreate(final View view, final boolean hasChangeVisibility) {
        view.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                mSizeFillWidth = view.getWidth();

                if(hasChangeVisibility) {
                    if(mViewResizeFill.getWidth() == 0) {
                        mTextViewLabelOut.setVisibility(View.GONE);
                        mTextViewLabelIn.setVisibility(View.GONE);
                    }else if(mViewResizeFill.getWidth() > 100){
                        mTextViewLabelOut.setVisibility(View.GONE);
                        mTextViewLabelIn.setVisibility(View.VISIBLE);
                    }else {
                        mTextViewLabelOut.setVisibility(View.VISIBLE);
                        mTextViewLabelIn.setVisibility(View.GONE);
                    }
                }

            }
        });
    }

    private void setValueLabelText(int value) {
        String labelPercentage = String.format(" %s %%", value);
        mTextViewLabelIn.setText(labelPercentage);
        mTextViewLabelOut.setText(labelPercentage);
    }

}
