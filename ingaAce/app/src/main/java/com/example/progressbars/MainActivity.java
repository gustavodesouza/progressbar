package com.example.progressbars;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.progressbars.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private ActivityMainBinding mVwBinding;
    private int valor = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mVwBinding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(mVwBinding.getRoot());

        mVwBinding.button1.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        AceProgressBar aceProgressBar = new AceProgressBar(this);
        aceProgressBar.createView(mVwBinding.getRoot(), mVwBinding.viewBorderProgress);
        valor += 15;
        mVwBinding.viewBorderProgress.post(() -> aceProgressBar.updateUI(valor));

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mVwBinding.viewBorderProgress.setVisibility(View.GONE);

                reaparece();
            }
        }, 2000);
    }

    private void reaparece() {
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mVwBinding.viewBorderProgress.setVisibility(View.VISIBLE);
            }
        }, 1000);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(MainActivity.this, MainActivity2.class);
        startActivity(intent);
    }
}